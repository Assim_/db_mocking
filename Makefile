server:
	go run main.go

test:
	go test -cover -v ./...

mock:
	mockgen -package mock -destination internal/repo/mock/language.go user/internal/repo LanguageRepoImply
