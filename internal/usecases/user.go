package usecases

import (
	"context"
	"fmt"
	"user/internal/entities"
	"user/internal/repo"
	"user/utilities"

	"github.com/google/uuid"
)

type UserUseCases struct {
	repo repo.UserRepoImply
}

type UserUseCaseImply interface {
	CreateUser(ctx context.Context, user entities.User) (string, error)
	GetUsers(ctx context.Context, page, limit int32) ([]*entities.User, error)
	DeleteUser(ctx context.Context, id string) error
}

// NewUserUseCases
func NewUserUseCases(userRepo repo.UserRepoImply) UserUseCaseImply {
	return &UserUseCases{
		repo: userRepo,
	}
}

func (userUseCase *UserUseCases) CreateUser(ctx context.Context, user entities.User) (string, error) {
	if utilities.IsEmpty(user.Name) {
		return "", fmt.Errorf("name cannot be empty ")
	}
	if !utilities.IsValidEmail(user.Email) {
		return "", fmt.Errorf("invalid email address")
	}

	uID, err := userUseCase.repo.CreateUser(ctx, user)
	return uID, err

}
func (userUseCase *UserUseCases) GetUsers(ctx context.Context, page, limit int32) ([]*entities.User, error) {
	if page <= 0 {
		page = 1
	}
	if limit <= 0 {
		limit = 10
	}
	users, err := userUseCase.repo.GetUsers(ctx, page, limit)
	return users, err

}
func (userUseCase *UserUseCases) DeleteUser(ctx context.Context, id string) error {

	uID, err := uuid.Parse(id)
	if err != nil {
		return fmt.Errorf("invalid id")
	}
	err = userUseCase.repo.DeleteUser(ctx, uID)
	return err

}
