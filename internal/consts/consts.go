package consts

const (
	DatabaseType     = "postgres"
	AppName          = "user"
	AcceptedVersions = "v1.0"
)

const (
	ContextAcceptedVersions       = "Accept-Version"
	ContextSystemAcceptedVersions = "System-Accept-Versions"
	ContextAcceptedVersionIndex   = "Accepted-Version-index"
)
