package controllers

import (
	"net/http"
	"user/internal/entities"
	"user/internal/usecases"

	"user/version"

	"github.com/gin-gonic/gin"
)

type UserController struct {
	router   *gin.RouterGroup
	useCases usecases.UserUseCaseImply
}

// NewUserController
func NewUserController(router *gin.RouterGroup, userUsecase usecases.UserUseCaseImply) *UserController {
	return &UserController{
		router:   router,
		useCases: userUsecase,
	}
}

// InitRoutes
func (language *UserController) InitRoutes() {

	language.router.GET("/:version/users", func(ctx *gin.Context) {
		version.RenderHandler(ctx, language, "GetUsers")
	})
	language.router.POST("/:version/users", func(ctx *gin.Context) {
		version.RenderHandler(ctx, language, "CreateUser")
	})
	language.router.DELETE("/:version/users/:id", func(ctx *gin.Context) {
		version.RenderHandler(ctx, language, "DelUser")
	})
}

func (userCtrl *UserController) CreateUser(ctx *gin.Context) {

	var user entities.User
	if err := ctx.BindJSON(&user); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"message": "invalid json data",
			"error":   err.Error(),
		})
		return
	}

	res, err := userCtrl.useCases.CreateUser(ctx, user)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"message": "something went wrong",
			"error":   err.Error(),
		})
		return
	}
	ctx.JSON(http.StatusCreated, gin.H{
		"data": res,
	})

}
func (userCtrl *UserController) GetUsers(ctx *gin.Context) {
	var params entities.Params
	if err := ctx.BindQuery(&params); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"message": "invalid query params",
			"error":   err.Error(),
		})
		return
	}

	resp, err := userCtrl.useCases.GetUsers(ctx, params.Page, params.Limit)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"message": "something went wrong",
			"error":   err.Error(),
		})
		return
	}
	ctx.JSON(http.StatusOK, gin.H{
		"data": resp,
	})
}
func (userCtrl *UserController) DelUser(ctx *gin.Context) {

	id := ctx.Param("id")
	err := userCtrl.useCases.DeleteUser(ctx, id)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"message": "something went wrong",
			"error":   err.Error(),
		})
		return
	}
	ctx.JSON(http.StatusOK, gin.H{
		"message": "user deleted successfully",
	})
}
