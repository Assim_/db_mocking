package repo

import (
	"context"
	"database/sql"
	"user/internal/entities"

	"github.com/google/uuid"
)

type UserRepo struct {
	db *sql.DB
}

type UserRepoImply interface {
	CreateUser(ctx context.Context, user entities.User) (string, error)
	GetUsers(ctx context.Context, page, limit int32) ([]*entities.User, error)
	DeleteUser(ctx context.Context, id uuid.UUID) error
}

// NewUserRepo
func NewUserRepo(db *sql.DB) UserRepoImply {
	return &UserRepo{db: db}
}

func (userRepo *UserRepo) CreateUser(ctx context.Context, user entities.User) (string, error) {

	var id string
	createUserStmt := `
		INSERT INTO public.users(name, email)
		VALUES($1, $2)
		Returning id
	`
	res := userRepo.db.QueryRowContext(ctx, createUserStmt, user.Name, user.Email)
	if err := res.Scan(&id); err != nil {
		return "", err
	}
	return id, nil
}

func (userRepo *UserRepo) GetUsers(ctx context.Context, page, limit int32) ([]*entities.User, error) {

	var users []*entities.User
	getUSers := `
		SELECT * 
		From public.users
		Limit $1 OFFSET $2
	`
	offset := (page - 1) * limit
	res, err := userRepo.db.QueryContext(ctx, getUSers, limit, offset)
	if err != nil {
		return nil, err
	}
	defer res.Close()
	for res.Next() {
		var i entities.User
		if err := res.Scan(
			&i.ID,
			&i.Name,
			&i.Email,
		); err != nil {
			return nil, err
		}
		users = append(users, &i)
	}
	if err := res.Err(); err != nil {
		return nil, err
	}
	return users, nil
}

func (userRepo *UserRepo) DeleteUser(ctx context.Context, id uuid.UUID) error {

	delUser := `
		DELETE FROM public.users 
		WHERE id=$1
	`
	_, err := userRepo.db.ExecContext(ctx, delUser, id)
	return err
}
