package entities

type MetaData struct {
	Total       int64 `json:"total"`
	PerPage     int32 `json:"per_page"`
	CurrentPage int32 `json:"current_page"`
	Next        int32 `json:"next"`
	Prev        int32 `json:"prev"`
}

type Response struct {
	StatusCode int         `json:"status_code"`
	Message    string      `json:"message"`
	MetaData   *MetaData   `json:"meta_data,omitempty"`
	Data       interface{} `json:"data,omitempty"`
}

type ErrorResponse struct {
	Message   string                 `json:"message"`
	ErrorCode interface{}            `json:"errorCode"`
	Errors    map[string]interface{} `json:"errors"`
}
