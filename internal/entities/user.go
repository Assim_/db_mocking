package entities

import "github.com/google/uuid"

type User struct {
	ID    uuid.UUID `json:"id,omitempty"`
	Name  string    `json:"name"`
	Email string    `json:"email"`
}

type Params struct {
	Limit int32 `form:"limit"`
	Page  int32 `form:"page"`
}
